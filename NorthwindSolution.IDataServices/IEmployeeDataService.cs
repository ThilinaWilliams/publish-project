﻿using NorthwindSolution.Entities;
using System.Collections.Generic;

namespace NorthwindSolution.IDataServices
{
    /// <summary>
    /// Employee data service
    /// </summary>
    public interface IEmployeeDataService
    {
        List<Employees> GetEmployees();
    }
}
