﻿using NorthwindSolution.DataServices;
using NorthwindSolution.IDataServices;
using NorthwindSolution.IServices;
using NorthwindSolution.Services;
using Unity;
using Unity.Lifetime;

namespace NorthwindSolution.Core.Dependency
{
    /// <summary>
    /// IOC container
    /// </summary>
    public static class PosIoCRegister
    {
        /// <summary>
        /// Registers all.
        /// </summary>
        /// <param name="container">The container.</param>
        public static void RegisterAll(IUnityContainer container)
        {
            RegisterDataServices(container);
            RegisterServices(container);
        }

        /// <summary>
        /// Registers the business services.
        /// </summary>
        /// <param name="container">The container.</param>
        public static void RegisterServices(IUnityContainer container)
        {
           
            container.RegisterType<IEmployeeService, EmployeeService>(new HierarchicalLifetimeManager());

        }

        /// <summary>
        /// Registers the data services.
        /// </summary>
        /// <param name="container">The container.</param>
        public static void RegisterDataServices(IUnityContainer container)
        {
           
            container.RegisterType<IEmployeeDataService, EmployeeDataService>(new HierarchicalLifetimeManager());

        }
    }
}
