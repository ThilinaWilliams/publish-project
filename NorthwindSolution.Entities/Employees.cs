﻿namespace NorthwindSolution.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Employees
    {
        public int EmployeeID { get; set; } //Get or sets Employee id
        public string FirstName { get; set; }   //Get or sets FirstName
        public string LastName { get; set; }    //Get or sets LastName
        public string PostalCode { get; set; }  //Get or sets PostalCode
        public string Country { get; set; } //Get or sets Country
    }
}
